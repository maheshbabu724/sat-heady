#Task 1

```
Find the Jenkins job template, in task1 fine.
I have authenticated docker registry, before following steps.
I have create CI,CD pipeline from GitLab Project (nodejs with mongodb). Containerised as well.
Gitlab Url : https://gitlab.com/maheshbabu724/nodejs-sampleproject
nginx load balancer also, we can create manually and point those to port mapping.
(or)
we can use deployment in kubernetes as well. 
Find the deployment script in kubernetesdeployment.yml, where I deployed Pod, services and Ingress controller. 
```

--------
#Task 2


```
#While deploying, if files were opened in that folder. It detects and closes the file.
for i in $(lsof +D /etc/nginx/html/ | awk '{print $2}')
do
		(gdb) p close($i)
         echo "PID $i Closed"
done
```


--------
#Task 3

```
#first clean
sudo apt-get clean
#Identifing more space taken folders
du -sm * | sort -rn | head -10 
#Since, space is full. attach disk and make new dir
mkdir -p /home/share/doc 
#Tranfer files to other mounted path or other place in host
mv /usr/share/doc/* /home/share/doc 
#Remove the directory, that occupied more space
rmdir /usr/share/doc 
#Create hard link
ln -s /home/share/doc /usr/share/doc  
```
-----------